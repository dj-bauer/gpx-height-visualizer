#!/usr/bin/env python3
import sys
import math

def get_log(name : str, color : int) -> str:
    return "[\033[" + str(color) + "m" + name + "\033[0m] "

def log_error() -> str:
    return get_log("Error", 31)

def log_info() -> str:
    return get_log("Info", 34)

def log_result() -> str:
    return get_log("Result", 32)

try:
    import gpxpy
except:
    print(log_error() + "Please install gpxpy")
    print("[Help]      e.g. pip3 install gpxpy")
    exit(1)
try:
    import matplotlib
    import matplotlib.pyplot as plt
except:
    print(log_error() + "Please install matplotlib")
    print("[Help]      e.g. pip3 install matplotlib")
    exit(1)


try:
    filename = sys.argv[1]
except:
    print(log_error() + "Please provide a filename")
    exit(1)

print(log_info() + "Loading file. It might take a while depending on your track")
gpx=gpxpy.parse(open(filename))

points = []
for track in gpx.tracks:
    for segment in track.segments:
        for point in segment.points:
            points.append(point)

print(log_info() + "All data loaded")
print(log_info() + str(len(points)) + " points found")

first=points[0].time

x_values = []
y_values = []

way=0

for i in range(0, len(points)-1):
    point=points[i]

    if i==0:
        continue
    #td = (point.time - first).total_seconds() / 3600 # In hours
    wd=point.distance_3d(points[i-1])/1000
    way += wd

    ele = point.elevation
    
    x=way
    x_values.append(x)
    y_values.append(ele)


m_up, m_down = gpx.get_uphill_downhill()
m_up = math.floor(m_up)
m_down = math.floor(m_down)
print(log_result() + "{}m up and {}m down ".format(m_up, m_down))

length = gpx.length_3d()
length = math.floor(length)
length_km = round(length/1000, 2)
print(log_result() + "{}m total walked {}km".format(length, length_km))

fig, graph = plt.subplots()
graph.plot(x_values, y_values)
graph.set_xlabel("Way in km")
graph.set_ylabel("Height in m")
graph.text(20, 450, "Total:")
#graph.text(20, 400, "⬆{}m ⬇{}m".format(m_up, m_down))
graph.text(20, 400, "{}km far".format(length_km))
plt.show()

try:
    fname=sys.argv[2]
    fig.savefig(fname)
except Exception as e:
    pass

